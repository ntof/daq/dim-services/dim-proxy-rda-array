include(Tools)

if(NOT BOOST_ROOT AND IS_DIRECTORY "/opt/cmw-core")
    message(STATUS "Using preferred boost version: /opt/cmw-core")
    set(BOOST_ROOT "/opt/cmw-core" CACHE PATH "")
endif()

find_package(Boost REQUIRED system thread chrono filesystem atomic regex log log_setup)

find_library(PTHREAD_LIBRARIES pthread)
if(PTHREAD_LIBRARIES)
    list(APPEND Boost_LIBRARIES ${PTHREAD_LIBRARIES})
endif()

# for interprocess
find_library(RT_LIBRARIES rt)
if(RT_LIBRARIES)
    list(APPEND Boost_LIBRARIES ${RT_LIBRARIES})
endif()
