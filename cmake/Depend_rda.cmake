
find_path(RDA_INCLUDE_DIRS "cmw-rda3/client/service/ClientService.h" PATHS "/opt/cmw-core/include/cmw-core")
find_library(CMW_DATA_LIBRARIES "cmw-data" PATHS "/opt/cmw-core/lib64")
find_library(CMW_DC_LIBRARIES "cmw-directory-client" PATHS "/opt/cmw-core/lib64")
find_library(CMW_FWK_LIBRARIES "cmw-fwk" PATHS "/opt/cmw-core/lib64")
find_library(CMW_LOG_LIBRARIES "cmw-log" PATHS "/opt/cmw-core/lib64")
find_library(CMW_RBAC_LIBRARIES "cmw-rbac" PATHS "/opt/cmw-core/lib64")
find_library(CMW_RDA3_LIBRARIES "cmw-rda3" PATHS "/opt/cmw-core/lib64")
find_library(CMW_UTIL_LIBRARIES "cmw-util" PATHS "/opt/cmw-core/lib64")


find_library(GSS_KRB5_LIBRARIES "gssapi_krb5" PATHS "/usr/lib64")

set(RDA_LIBRARIES
        ${CMW_FWK_LIBRARIES} ${CMW_RBAC_LIBRARIES}
        ${CMW_RDA3_LIBRARIES} ${CMW_DC_LIBRARIES}
        ${CMW_DATA_LIBRARIES} ${CMW_LOG_LIBRARIES}
        ${CMW_UTIL_LIBRARIES} ${GSS_KRB5_LIBRARIES})

find_library(ZMQ_LIBRARIES "zmq" PATHS "/opt/cmw-core/lib")
find_path(ZMQ_INCLUDE_DIRS "zmq.h" PATHS "/opt/cmw-core/include")

find_library(CURL_LIBRARIES "curl")
find_library(CRYPTO_LIBRARIES "crypto")

assert(CURL_LIBRARIES MESSAGE "curl library missing")
assert(CRYPTO_LIBRARIES MESSAGE "crypto library missing")

list(APPEND RDA_LIBRARIES ${ZMQ_LIBRARIES} ${CURL_LIBRARIES} ${CRYPTO_LIBRARIES})
list(APPEND RDA_INCLUDE_DIRS ${ZMQ_INCLUDE_DIRS})

assert(RDA_INCLUDE_DIRS AND RDA_LIBRARIES MESSAGE "cmw-core package missing")
