# dim-proxy-rda-array

RDA array proxy for DIM.

**Note:** this component is **not** a dim-proxy plugin, but a standalone component,
it provides plain DIM services rather than DataSets.

# Configuration

```XML
element proxy {
  element serverName {
    # DIM server name (default: dim-proxy-rda-array)
    attribute value { xsd:string }
  } ?,
  element sources {
    element source {
      # RDA device to connect to
      attribute device { xsd:string },
      # RDA property to retrieve
      attribute property { xsd:string },
      # RDA Selector
      attribute selector { xsd:string }
      
      element fields {
        element field {
          # Name of the field to forward
          attribute name { xsd:string },
          # Whereas multi-dimension array should be flattened (default:false)
          # ex: D:10 vs D:5;D:5 flattening a 2D array with 5 elements
          attribute flatten { "true" | "false" } ?
        } *
      }
    } *
  }
}
```

## Build

To build this project it is recommended to install [docker-builder](https://gitlab.cern.ch/mro/common/tools/x-builder) script.

```bash
docker-builder

# This package requires some additional dependencies (RDA related)
yum install -y curl-devel cmw-core openssl-devel

rm -rf build && mkdir build
cd build && cmake3 .. -DTESTS=ON

make -j4
```

## Testing

```bash
# Run tests
docker-builder make -C build tests

# Run clang-format
docker-builder make -C build style

# Run linters
docker-builder make -C build lint
```

## Running

To run this service on a local DIM/DNS node, edit `tests/data/sysmon.xml` to set the DIM/DNS address.

Then to connect the service from a container to this DNS:
```bash
docker-builder
export DIM_HOST_NODE=$(hostname -i)

./build/src/dim-proxy-rda -c ./tests/data/proxy-rda.xml -m ./tests/data/misc.xml
```

## Debugging

To compile this component for gdb/lldb:
```bash
docker-builder

cd build && cmake3 -DCMAKE_BUILD_TYPE=Debug ..
```
