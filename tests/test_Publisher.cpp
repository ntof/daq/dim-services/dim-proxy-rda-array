/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Publisher.hpp"
#include "test_helpers.hpp"

using namespace ntof::proxy;
using namespace ntof;

class TestPublisher : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestPublisher);
    CPPUNIT_TEST(array_bool);
    CPPUNIT_TEST(array_byte);
    CPPUNIT_TEST(array_short);
    CPPUNIT_TEST(array_int);
    CPPUNIT_TEST(array_long);
    CPPUNIT_TEST(array_float);
    CPPUNIT_TEST(array_double);
    CPPUNIT_TEST(array_resize);
    CPPUNIT_TEST(array_flatten);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() override { m_dim.reset(new DimTestHelper()); }

    void tearDown() override { m_dim.reset(); }

    template<typename Type>
    void checkDimInfoValues(DimInfoWaiter &info,
                            Type expectedValue,
                            int expectedSize)
    {
        // Check Data
        int size = info.getSize() / sizeof(Type);
        char *data = (char *) info.getData();
        const Type *values = reinterpret_cast<const Type *>(data);
        EQ(expectedSize, size);
        for (int i = 0; i < size; i++)
        {
            if (std::is_same<Type, double>::value)
            {
                EQ_DBL(expectedValue, values[i], 0.1);
            }
            else
            {
                EQ(expectedValue, values[i]);
            }
        }
    }

    void array_bool()
    {
        bool values[6] = {true, true, true, true, true, true};

        // Single Array
        std::string serviceName = "Device/Acquisition/testField_bool";
        Publisher publisher = Publisher(serviceName);

        publisher.postArrayData<int8_t>(
            reinterpret_cast<const int8_t *>(values), 1, 6);
        DimInfoWaiter info(serviceName.c_str());
        EQ(true, info.waitUpdate());
        checkDimInfoValues(info, int8_t(true), int(6));
        EQ(std::string("C:6"), std::string(info.getFormat()));

        // 2D Array
        std::string serviceName2D = "Device/Acquisition/testField_bool2D";
        Publisher publisher2D = Publisher(serviceName2D);
        publisher2D.postArrayData<int8_t>(
            reinterpret_cast<const int8_t *>(values), 2, 3);
        DimInfoWaiter info2D(serviceName2D.c_str());
        EQ(true, info2D.waitUpdate());
        checkDimInfoValues(info2D, int8_t(true), int(2 * 3));
        EQ(std::string("C:3;C:3"), std::string(info2D.getFormat()));
    }

    void array_byte()
    {
        int8_t values[6] = {0, 0, 0, 0, 0, 0};

        // Single Array
        std::string serviceName = "Device/Acquisition/testField_byte";
        Publisher publisher = Publisher(serviceName);

        publisher.postArrayData<int8_t>(
            reinterpret_cast<const int8_t *>(values), 1, 6);
        DimInfoWaiter info(serviceName.c_str());
        EQ(true, info.waitUpdate());
        checkDimInfoValues(info, int8_t(0), int(6));
        EQ(std::string("C:6"), std::string(info.getFormat()));

        // 2D Array
        std::string serviceName2D = "Device/Acquisition/testField_byte2D";
        Publisher publisher2D = Publisher(serviceName2D);
        publisher2D.postArrayData<int8_t>(
            reinterpret_cast<const int8_t *>(values), 2, 3);
        DimInfoWaiter info2D(serviceName2D.c_str());
        EQ(true, info2D.waitUpdate());
        checkDimInfoValues(info2D, int8_t(0), int(2 * 3));
        EQ(std::string("C:3;C:3"), std::string(info2D.getFormat()));
    }

    void array_short()
    {
        int16_t values[6] = {0, 0, 0, 0, 0, 0};

        // Single Array
        std::string serviceName = "Device/Acquisition/testField_short";
        Publisher publisher = Publisher(serviceName);

        publisher.postArrayData<int16_t>(values, 1, 6);
        DimInfoWaiter info(serviceName.c_str());
        EQ(true, info.waitUpdate());
        checkDimInfoValues(info, int16_t(0), int(6));
        EQ(std::string("S:6"), std::string(info.getFormat()));

        // 2D Array
        std::string serviceName2D = "Device/Acquisition/testField_short2D";
        Publisher publisher2D = Publisher(serviceName2D);
        publisher2D.postArrayData<int16_t>(values, 2, 3);
        DimInfoWaiter info2D(serviceName2D.c_str());
        EQ(true, info2D.waitUpdate());
        checkDimInfoValues(info2D, int16_t(0), int(2 * 3));
        EQ(std::string("S:3;S:3"), std::string(info2D.getFormat()));
    }

    void array_int()
    {
        int32_t values[6] = {0, 0, 0, 0, 0, 0};

        // Single Array
        std::string serviceName = "Device/Acquisition/testField_int";
        Publisher publisher = Publisher(serviceName);

        publisher.postArrayData<int32_t>(values, 1, 6);
        DimInfoWaiter info(serviceName.c_str());
        EQ(true, info.waitUpdate());
        checkDimInfoValues(info, int32_t(0), int(6));
        EQ(std::string("I:6"), std::string(info.getFormat()));

        // 2D Array
        std::string serviceName2D = "Device/Acquisition/testField_int2D";
        Publisher publisher2D = Publisher(serviceName2D);
        publisher2D.postArrayData<int32_t>(values, 2, 3);
        DimInfoWaiter info2D(serviceName2D.c_str());
        EQ(true, info2D.waitUpdate());
        checkDimInfoValues(info2D, int32_t(0), int(2 * 3));
        EQ(std::string("I:3;I:3"), std::string(info2D.getFormat()));
    }

    void array_long()
    {
        int64_t values[6] = {0, 0, 0, 0, 0, 0};

        // Single Array
        std::string serviceName = "Device/Acquisition/testField_long";
        Publisher publisher = Publisher(serviceName);

        publisher.postArrayData<int64_t>(values, 1, 6);
        DimInfoWaiter info(serviceName.c_str());
        EQ(true, info.waitUpdate());
        checkDimInfoValues(info, int64_t(0), int(6));
        EQ(std::string("X:6"), std::string(info.getFormat()));

        // 2D Array
        std::string serviceName2D = "Device/Acquisition/testField_long2D";
        Publisher publisher2D = Publisher(serviceName2D);
        publisher2D.postArrayData<int64_t>(values, 2, 3);
        DimInfoWaiter info2D(serviceName2D.c_str());
        EQ(true, info2D.waitUpdate());
        checkDimInfoValues(info2D, int64_t(0), int(2 * 3));
        EQ(std::string("X:3;X:3"), std::string(info2D.getFormat()));
    }

    void array_float()
    {
        float values[6] = {0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f};

        // Single Array
        std::string serviceName = "Device/Acquisition/testField_float";
        Publisher publisher = Publisher(serviceName);

        publisher.postArrayData<float>(values, 1, 6);
        DimInfoWaiter info(serviceName.c_str());
        EQ(true, info.waitUpdate());
        checkDimInfoValues(info, float(0), int(6));
        EQ(std::string("F:6"), std::string(info.getFormat()));

        // 2D Array
        std::string serviceName2D = "Device/Acquisition/testField_float2D";
        Publisher publisher2D = Publisher(serviceName2D);
        publisher2D.postArrayData<float>(values, 2, 3);
        DimInfoWaiter info2D(serviceName2D.c_str());
        EQ(true, info2D.waitUpdate());
        checkDimInfoValues(info2D, float(0), int(2 * 3));
        EQ(std::string("F:3;F:3"), std::string(info2D.getFormat()));
    }

    void array_double()
    {
        double values[6] = {0, 0, 0, 0, 0, 0};

        // Single Array
        std::string serviceName = "Device/Acquisition/testField_double";
        Publisher publisher = Publisher(serviceName);

        publisher.postArrayData<double>(values, 1, 6);
        DimInfoWaiter info(serviceName.c_str());
        EQ(true, info.waitUpdate());
        checkDimInfoValues(info, double(0), int(6));
        EQ(std::string("D:6"), std::string(info.getFormat()));

        // 2D Array
        std::string serviceName2D = "Device/Acquisition/testField_double2D";
        Publisher publisher2D = Publisher(serviceName2D);
        publisher2D.postArrayData<double>(values, 2, 3);
        DimInfoWaiter info2D(serviceName2D.c_str());
        EQ(true, info2D.waitUpdate());
        checkDimInfoValues(info2D, double(0), int(2 * 3));
        EQ(std::string("D:3;D:3"), std::string(info2D.getFormat()));
    }

    void array_resize()
    {
        int32_t values_before[3] = {0, 0, 0};
        int32_t values_after[6] = {0, 0, 0, 0, 0, 0};

        // Start with a single Array
        std::string serviceName = "Device/Acquisition/testField_resize";
        Publisher publisher = Publisher(serviceName);
        publisher.postArrayData<int32_t>(values_before, 1, 3);
        DimInfoWaiter info(serviceName.c_str());
        EQ(true, info.waitUpdate());
        checkDimInfoValues(info, int32_t(0), int(3));
        EQ(std::string("I:3"), std::string(info.getFormat()));

        // Now push 2D Array
        publisher.postArrayData<int32_t>(values_after, 2, 3);
        DimInfoWaiter info2D(serviceName.c_str());
        EQ(true, info2D.waitUpdate());
        checkDimInfoValues(info2D, int32_t(0), int(2 * 3));
        EQ(std::string("I:3;I:3"), std::string(info2D.getFormat()));
    }

    void array_flatten()
    {
        double values[6] = {0, 0, 0, 0, 0, 0};

        // 2D Array with flattening
        std::string serviceName2D = "Device/Acquisition/"
                                    "testField_double2D_Flatten";
        Publisher publisher2D = Publisher(serviceName2D, true);
        publisher2D.postArrayData<double>(values, 2, 3);
        DimInfoWaiter info2D(serviceName2D.c_str());
        EQ(true, info2D.waitUpdate());
        checkDimInfoValues(info2D, double(0), int(2 * 3));
        // It must be D:6 instead of D:3;D:3
        EQ(std::string("D:6"), std::string(info2D.getFormat()));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestPublisher);
