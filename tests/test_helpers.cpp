/*
** Copyright (C) 2018 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-10-09T13:29:47+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "test_helpers.hpp"

#include <signal.h>
#include <spawn.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void assertContains(const std::string &haystack,
                    const std::string &needle,
                    const CPPUNIT_NS::SourceLine &line)
{
    CPPUNIT_NS::Asserter::failIf(
        haystack.find(needle) == std::string::npos,
        CPPUNIT_NS::Message("assertion failed",
                            "Expected  : \"" + haystack + "\"",
                            "To contain: \"" + needle + "\""),
        line);
}

pid_t aspawn(const char *const argv[])
{
    pid_t pid;
    posix_spawn_file_actions_t actions;

    if (posix_spawn_file_actions_init(&actions) != 0)
        return -1;
    posix_spawn_file_actions_addclose(&actions, STDIN_FILENO);

    if (posix_spawn(&pid, argv[0], &actions, NULL, (char *const *) (argv),
                    environ) != 0)
    {
        pid = -1;
    }
    posix_spawn_file_actions_destroy(&actions);
    return pid;
}

void terminate(pid_t pid)
{
    int stat_loc;
    if (pid < 0)
        return;

    kill(pid, SIGTERM);
    boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
    if (waitpid(pid, &stat_loc, WNOHANG) == -1)
    {
        kill(pid, SIGKILL);
        waitpid(pid, &stat_loc, 0);
    }
}
