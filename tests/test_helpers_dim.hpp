/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-01-06T16:28:01+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef TEST_HELPERS_DIM_HPP__
#define TEST_HELPERS_DIM_HPP__

#include <boost/thread.hpp>

#include <sys/types.h>
#include <unistd.h>

#include <dic.hxx>

class DimInfoWaiter : public DimInfo
{
public:
    explicit DimInfoWaiter(const char *name);

    void reset();
    bool waitUpdate(unsigned int count = 1, unsigned int ms = 1000);

    virtual void infoHandler();

protected:
    unsigned int m_count;
    boost::condition_variable m_cond;
    boost::mutex m_lock;
};

class DimTestHelper
{
public:
    DimTestHelper();
    ~DimTestHelper();

protected:
    pid_t m_pid;
};

#endif
