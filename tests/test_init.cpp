/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-01-07T08:50:32+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <boost/filesystem.hpp>

#include <stdlib.h>

#include "Config.hpp"
#include "local-config.h"

using namespace ntof::proxy;
namespace bfs = boost::filesystem;

void test_init()
{
    ::setenv("DIM_DNS_PORT", "12345", 1);
    ::setenv("DIM_DNS_NODE", "127.0.0.1", 1);
    ::setenv("DIM_HOST_NODE", "127.0.0.1", 1);

    const bfs::path dataDir = bfs::path(SRCDIR) / "tests" / "data";
    Config::load((dataDir / "config.xml").string(),
                 (dataDir / "configMisc.xml").string());
}
