/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Config.hpp"
#include "Exception.hpp"
#include "local-config.h"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::proxy;
using namespace ntof;

class TestConfig : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestConfig);
    CPPUNIT_TEST(test_Config_Exceptions);
    CPPUNIT_TEST(test_FullConfig);
    CPPUNIT_TEST_SUITE_END();

protected:
    bfs::path dataDir_;

public:
    void setUp() override { dataDir_ = bfs::path(SRCDIR) / "tests" / "data"; }

    void tearDown() override
    {
        Config::load((dataDir_ / "config.xml").string(),
                     (dataDir_ / "configMisc.xml").string());
    }

    void test_Config_Exceptions()
    {
        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "config_bad_source.xml").string(),
                         (dataDir_ / "configMisc.xml").string()),
            ntof::proxy::Exception);

        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "config_bad_source_no_fields.xml").string(),
                         (dataDir_ / "configMisc.xml").string()),
            ntof::proxy::Exception);

        CPPUNIT_ASSERT_THROW(
            Config::load((dataDir_ / "no_existing_file.xml.xml").string(),
                         (dataDir_ / "configMisc.xml").string()),
            ntof::proxy::Exception);
    }

    void test_FullConfig()
    {
        Config &conf = Config::instance();
        EQ(std::string("dim-proxy-rda-array"), conf.getServerName());

        const SourceConfigList &sources = conf.getSources();
        EQ(size_t(1), sources.size());
        EQ(size_t(2), sources.front().fields.size());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestConfig);
