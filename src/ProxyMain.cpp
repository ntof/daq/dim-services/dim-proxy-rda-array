/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include "ProxyMain.hpp"

#include <Source.hpp>

#include "Config.hpp"

#include <dic.hxx>

namespace ntof {
namespace proxy {

ProxyMain::~ProxyMain()
{
    stop();
    m_sources.clear();
    Source::stopCMWServices();
}

void ProxyMain::initSources()
{
    m_sources.clear();
    for (const SourceConfig &config : Config::instance().getSources())
    {
        config.log();
        m_sources.emplace_back(new Source(config));
    }
}

void ProxyMain::thread_func()
{
    if (m_sources.empty())
        initSources();

    while (m_started.load())
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

} // namespace proxy
} // namespace ntof
