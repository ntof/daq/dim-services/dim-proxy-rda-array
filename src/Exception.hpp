/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef EXCEPTION_HPP
#define EXCEPTION_HPP

#include <NTOFException.h>

namespace ntof {
namespace proxy {

#define PROXY_THROW(msg, code) throw Exception(msg, __FILE__, __LINE__, code)

/**
 * @class Exception
 * @brief Exception throw by the addh
 */
class Exception : public NTOFException
{
public:
    /**
     * @brief Constructor of the class
     * @param Msg Error message
     * @param file Name of the file where the exception is thrown (__FILE__)
     * @param Line Line of code doing the error (__LINE__)
     */
    Exception(const std::string &Msg,
              const std::string &file,
              int Line,
              int error = 0);

    /**
     * @brief Constructor of the class
     * @param Msg Error message
     * @param file Name of the file where the exception is thrown (__FILE__)
     * @param Line Line of code doing the error (__LINE__)
     * @param error Error code
     */
    Exception(const char *Msg, const std::string &file, int Line, int error = 0);

    /**
     * @brief Destructor of the class
     */
    ~Exception() override = default;

    /**
     * @brief Overload of std::exception
     * @return Error message
     */
    virtual const char *what() const throw() override;

protected:
    mutable std::string m_what;
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* EXCEPTION_HPP */
