/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef PROXYMAIN_HPP
#define PROXYMAIN_HPP

#include <DaqTypes.h>
#include <Synchro.h>
#include <Thread.hpp>

#include "Types.hpp"

namespace ntof {
namespace proxy {

class Source;

class ProxyMain : public ntof::utils::Thread
{
public:
    typedef std::shared_ptr<Source> SourcePtr;

    ProxyMain() = default;
    ~ProxyMain() override;

protected:
    /**
     *  @brief utility to create Source classes
     */
    void initSources();

    /**
     * @brief Launch the thread
     */
    void thread_func() override;

    std::vector<SourcePtr> m_sources; //!< List of the clients
};

} // namespace proxy
} // namespace ntof

#endif /* PROXYMAIN_HPP */
