/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include "Config.hpp"

#include <map>
#include <string>

#include <pugixml.hpp>

#include "Exception.hpp"

#include <Singleton.hxx>

template class ntof::utils::Singleton<ntof::proxy::Config>;

namespace ntof {
namespace proxy {

const std::string Config::configFile = "/etc/ntof/dim-proxy-rda-array.xml";
const std::string Config::configMiscFile = "/etc/ntof/misc.xml";

Config::Config(const std::string &file, const std::string &miscFile) :
    ConfigMisc(miscFile)
{
    pugi::xml_document doc;

    // Read the config file
    pugi::xml_parse_result result = doc.load_file(file.c_str());
    if (!result)
    {
        PROXY_THROW("Unable to read the configuration file : " + file, 0);
    }
    else
    {
        // Parse the config file
        pugi::xml_node proxy = doc.child("proxy");
        // DIM service prefix

        pugi::xml_node serverNode = proxy.child("serverName");
        m_serverName = serverNode.attribute("value").as_string(
            "dim-proxy-rda-array");

        for (pugi::xml_node source = proxy.child("sources").first_child();
             source; source = source.next_sibling())
        {
            SourceConfig config;
            config.device = source.attribute("device").as_string("");
            config.property = source.attribute("property").as_string("");
            config.selector = source.attribute("selector").as_string("");

            for (pugi::xml_node field = source.child("fields").first_child();
                 field; field = field.next_sibling())
            {
                FieldConfig fieldConfig;
                fieldConfig.name = field.attribute("name").as_string("");
                fieldConfig.flatten = field.attribute("flatten").as_bool(false);
                if (!fieldConfig.name.empty())
                {
                    config.fields.push_back(fieldConfig);
                }
            }

            if (!config.isValid())
            {
                PROXY_THROW("Provided an invalid source configuration.", 0);
            }
            m_sources.push_back(config);
        }
    }
}

Config &Config::load(const std::string &file, const std::string &miscFile)
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
    {
        delete m_instance;
        m_instance = nullptr;
    }

    m_instance = new Config(file, miscFile);
    return *m_instance;
}

const std::string &Config::getServerName() const
{
    return m_serverName;
}

const SourceConfigList &Config::getSources() const
{
    return m_sources;
}

} /* namespace proxy */
} /* namespace ntof */
