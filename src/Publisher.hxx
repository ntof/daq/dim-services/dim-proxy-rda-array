/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef PUBLISHER_HXX
#define PUBLISHER_HXX

#include "Publisher.hpp"

#include <easylogging++.h>

namespace ntof {
namespace proxy {

template<typename SourceType>
void Publisher::postArrayData(const SourceType *array, size_t rows, size_t cols)
{
    // Iterate over rows
    int size = sizeof(SourceType) * rows * cols;

    // Calculate the dim service format string
    std::string formatService = getDimServiceFormat<SourceType>(rows, cols, m_flatten);
    if(formatService != m_dimServiceFormat)
    {
        // Format is different, reset and recreate DimService
        LOG(INFO) << "Service " << m_serviceName
                  << " changed format. From " << m_dimServiceFormat
                  << " to " << formatService;
        m_service.reset();
        m_dimServiceFormat = formatService;
    }

    if (m_service) // Service already initialized
    {
        m_service->updateService(array, size);
    }
    else // Create new DimService for this row
    {
        LOG(INFO) << "Creating new service for " << m_serviceName
                  << " with format " << m_dimServiceFormat;
        m_service.reset(new DimService(m_serviceName.c_str(),
                                       m_dimServiceFormat.c_str(), array, size));
    }
}

template<typename SourceType>
std::string Publisher::getDimServiceFormat(size_t rows, size_t cols, bool flatten)
{
    std::string format = "C"; // String by default for Char, byte, bool
    if (std::is_same<SourceType, int16_t>::value) {
        format = "S";
    } else if (std::is_same<SourceType, int32_t>::value) {
        format = "I";
    } else if (std::is_same<SourceType, int64_t>::value) {
        format = "X";
    } else if (std::is_same<SourceType, float>::value) {
        format = "F";
    } else if (std::is_same<SourceType, double>::value) {
        format = "D";
    }

    std::string formatService;
    if(flatten)
    {
        formatService += format + ":" + std::to_string(rows * cols);
    }
    else
    {
        for(size_t i = 0; i < rows; i++)
        {
            formatService += format + ":" + std::to_string(cols) + ";";
        }
        // Remove last ";"
        if (!formatService.empty()) {
            formatService.pop_back();
        }
    }
    return formatService;
}

} // namespace proxy
} // namespace ntof

#endif // PUBLISHER_HXX
