/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include <easylogging++.h>

#include "Config.hpp"
#include "Exception.hpp"
#include "ProxyMain.hpp"

#include <dic.hxx>
#include <dis.hxx>

using namespace ntof::proxy;

int main(int argc, char **argv)
{
    START_EASYLOGGINGPP(argc, argv);
    try
    {
        const std::string configFile = (argc > 1) ?
            std::string(argv[1]) :
            ntof::proxy::Config::configFile;
        const std::string configMiscFile = (argc > 2) ?
            std::string(argv[2]) :
            ntof::proxy::Config::configMiscFile;

        ntof::proxy::Config::load(configFile, configMiscFile);

        // Get the path of the config file of the logger
        std::string logConfFile = "/etc/ntof/nTOF_proxy-rda-array_logger.conf";
        if (argc > 3)
        {
            logConfFile = argv[3];
        }

        // Initialize logger
        el::Configurations confFromFile(
            logConfFile); // Load configuration from file
        el::Loggers::reconfigureAllLoggers(
            confFromFile); // Re-configures all the loggers to current
                           // configuration file

        // Initialized DIM parameters
        DimServer::setDnsNode(Config::instance().getDimDns().c_str());
        DimClient::setDnsNode(Config::instance().getDimDns().c_str());

        DimServer::start(Config::instance().getServerName().c_str());

        // Run ADDH Writer
        LOG(INFO) << "ProxyMain starting";
        ntof::proxy::ProxyMain proxyMain;
        proxyMain.run();
    }
    catch (const ntof::proxy::Exception &ex)
    {
        LOG(ERROR)
            << "[FATAL ERROR] Caught addh exception : " << ex.getMessage();
        return -1;
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught exception : " << ex.what();
        return -1;
    }
    catch (...)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught unknown exception";
        return -1;
    }

    return 0;
}
