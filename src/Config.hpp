/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef CONFIG_H_
#define CONFIG_H_

#include <map>
#include <string>
#include <vector>

#include <ConfigMisc.h>
#include <Singleton.hpp>
#include <easylogging++.h>
#include <sys/types.h>

#include "Types.hpp"

namespace ntof {
namespace proxy {

/**
 * @class Config
 * @brief Class used to read the config file
 */
class Config :
    public ntof::utils::ConfigMisc,
    public ntof::utils::Singleton<Config>
{
public:
    static const std::string configFile;     //!< Path of the config file
    static const std::string configMiscFile; //!< Path of the global config file

    /**
     * @brief Destructor of the class
     */
    ~Config() override = default;

    /**
     * @brief load configuration from the given files
     * @param[in] file the configuration file to load
     *
     * @details this method will destroy any existing Config and instantiate
     * the mutex again
     */
    static Config &load(const std::string &file, const std::string &miscFile);

    /**
     * @brief Get the name of the server
     * @return The name of the server
     */
    const std::string &getServerName() const;

    const SourceConfigList &getSources() const;

protected:
    friend class ntof::utils::Singleton<Config>;

    explicit Config(const std::string &file = configFile,
                    const std::string &miscFile = configMiscFile);

    SourceConfigList m_sources;
    std::string m_serverName;
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* CONFIG_H_ */
