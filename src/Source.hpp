/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef SOURCE_HPP
#define SOURCE_HPP

#include <string>

#include <cmw-fwk/client/rbac/RbacLoginService.h>
#include <cmw-rda3/client/service/ClientService.h>

#include "Publisher.hpp"
#include "Types.hpp"

#include <dis.hxx>

using namespace cmw::rda3::client;

namespace ntof {
namespace proxy {

class Source : public cmw::rda3::client::NotificationListener
{
public:
    typedef std::map<std::string, Publisher> ServicesMap;

    explicit Source(const SourceConfig &config);
    ~Source() override;

    void connect();

    /**
     * RDA call back
     * @param subscription
     * @param acqData
     * @param updateType
     */
    void dataReceived(const cmw::rda3::client::Subscription &subscription,
                      std::unique_ptr<cmw::rda3::common::AcquiredData> acqData,
                      cmw::rda3::common::UpdateType updateType) override;

    /**
     * RDA error call back
     * @param subscription
     * @param exception
     * @param updateType
     */
    void errorReceived(const cmw::rda3::client::Subscription &subscription,
                       std::unique_ptr<cmw::rda3::common::RdaException> exception,
                       cmw::rda3::common::UpdateType updateType) override;

    static void startCMWServices();
    static void stopCMWServices();

protected:
    SourceConfig m_config;
    ServicesMap m_services;

    SubscriptionSharedPtr subPtr;

    //!<< RDA client reference
    static std::unique_ptr<cmw::rda3::client::ClientService> client;
    //!<< RBAC login
    static std::unique_ptr<cmw::RbacLoginService> rbacService;
    //!<< service
    static bool cmwStarted;

    Source(const Source &) = delete;
    Source &operator=(const Source &) = delete;
};

} // namespace proxy
} // namespace ntof

#endif // SOURCE_HPP
