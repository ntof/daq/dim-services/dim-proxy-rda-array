/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef PUBLISHER_HPP
#define PUBLISHER_HPP

#include <memory>
#include <string>
#include <vector>

#include <dis.hxx>

namespace ntof {
namespace proxy {

class Publisher
{
public:
    typedef std::shared_ptr<DimService> DimServicePtr;

    explicit Publisher(const std::string &serviceName, bool flatten = false);
    ~Publisher() = default;

    template<typename SourceType>
    void postArrayData(const SourceType *array, size_t rows, size_t cols);

    template<typename SourceType>
    static std::string getDimServiceFormat(size_t rows,
                                           size_t cols,
                                           bool flatten);

protected:
    std::string m_serviceName;
    bool m_flatten;
    std::string m_dimServiceFormat;
    DimServicePtr m_service;
};

} // namespace proxy
} // namespace ntof

#include "Publisher.hxx"

#endif // PUBLISHER_HPP
