/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef TYPES_HPP
#define TYPES_HPP

#include <cstdint>
#include <string>
#include <vector>

#include <easylogging++.h>

namespace ntof {
namespace proxy {

struct FieldConfig
{
    std::string name;
    bool flatten;
};

struct SourceConfig
{
    std::string device;
    std::string property;
    std::string selector;
    std::vector<FieldConfig> fields;

    bool isValid() const
    {
        return !device.empty() && !property.empty() && !fields.empty();
    }

    void log() const
    {
        LOG(INFO) << "Proxy Source initialization:";
        LOG(INFO) << " - Device:    " << device;
        LOG(INFO) << " - Property:  " << property;
        if (!selector.empty())
            LOG(INFO) << " - Selector:  " << selector;

        std::ostringstream oss;
        for (const FieldConfig &field : fields)
        {
            oss << " " << field.name << " ";
        }
        LOG(INFO) << " - Fields:    " << oss.str();
    }
};

typedef std::vector<SourceConfig> SourceConfigList;

} // namespace proxy
} // namespace ntof

#endif
