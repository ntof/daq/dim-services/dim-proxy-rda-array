/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-13T14:43:44+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include "Source.hpp"

#include <cmw-data/DataExceptions.h>
#include <easylogging++.h>

namespace ntof {
namespace proxy {

std::unique_ptr<cmw::rda3::client::ClientService> Source::client; //!<< RDA
                                                                  //!< client
                                                                  //!< reference
std::unique_ptr<cmw::RbacLoginService> Source::rbacService; //!<< RBAC login
//!< service

bool Source::cmwStarted = false;

Source::Source(const SourceConfig &config) : m_config(config)
{
    connect();
}

Source::~Source()
{
    // stopCMWServices();
    subPtr->unsubscribe();
}

void Source::connect()
{
    startCMWServices();
    try
    {
        if (m_config.selector.empty())
        {
            subPtr = client->getAccessPoint(m_config.device, m_config.property)
                         .subscribe(NotificationListenerSharedPtr(this));
        }
        else
        {
            subPtr = client->getAccessPoint(m_config.device, m_config.property)
                         .subscribe(
                             cmw::rda3::common::RequestContextFactory::create(
                                 m_config.selector),
                             NotificationListenerSharedPtr(this));
        }

        LOG(INFO) << "Connected on " << m_config.device;
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR)
            << "Unable to connect to " << m_config.device << " : " << ex.what();
    }
}

void Source::startCMWServices()
{
    if (!cmwStarted)
    {
        if (client.get() == NULL)
        {
            client = cmw::rda3::client::ClientService::create();
        }
        rbacService.reset(new cmw::RbacLoginService(*client));
        rbacService->setLoginPolicy(rbac::LOCATION);
        rbacService->setApplicationName("nTOF info");
        rbacService->setAutoRefresh(true);
        rbacService->start();
        cmwStarted = true;
    }
}

void Source::stopCMWServices()
{
    if (cmwStarted)
    {
        rbacService->stop();
        rbacService.reset();
        cmwStarted = false;
    }
}
void Source::dataReceived(
    const Subscription & /*subscription*/,
    std::unique_ptr<cmw::rda3::common::AcquiredData> acqData,
    cmw::rda3::common::UpdateType /*updateType*/)
{
    const cmw::data::Data &data = acqData->getData();

    for (const FieldConfig &field : m_config.fields)
    {
        // Create field map if doesn't exist
        if (m_services.find(field.name) == m_services.end())
        {
            std::string serviceName = m_config.device + "/" +
                m_config.property + "/" + field.name;
            m_services.insert(
                {field.name, Publisher(serviceName, field.flatten)});
        }
        // Get vector of dim services (one per row)
        Publisher &publisher = m_services.at(field.name);

        const cmw::data::Entry *fieldEntry = data.getEntry(field.name);
        if (fieldEntry == NULL)
        {
            LOG(ERROR) << "Missing field " << field.name << " on "
                       << m_config.device << "/" << m_config.property;
            continue;
        }

        size_t rowCount, columnCount;

        switch (fieldEntry->getType())
        {
        /*********************************
         *  ARRAY
         *********************************/
        /** Boolean array. C++: bool[] */
        case cmw::data::DT_BOOL_ARRAY: {
            const bool *dataArray = fieldEntry->getArrayBool(columnCount);
            publisher.postArrayData(reinterpret_cast<const int8_t *>(dataArray),
                                    1, columnCount);
        }
        break;
        /** Byte array. C++: int8_t[] */
        case cmw::data::DT_BYTE_ARRAY: {
            const int8_t *dataArray = fieldEntry->getArrayByte(columnCount);
            publisher.postArrayData(reinterpret_cast<const char *>(dataArray),
                                    1, columnCount);
        }
        break;
        /** Short array. C++: int16_t[] */
        case cmw::data::DT_SHORT_ARRAY: {
            const int16_t *dataArray = fieldEntry->getArrayShort(columnCount);
            publisher.postArrayData(dataArray, 1, columnCount);
        }
        break;
        /** Int array. C++: int32_t[] */
        case cmw::data::DT_INT_ARRAY: {
            const int32_t *dataArray = fieldEntry->getArrayInt(columnCount);
            publisher.postArrayData(dataArray, 1, columnCount);
        }
        break;
        /** Long array. C++: int64_t[] */
        case cmw::data::DT_LONG_ARRAY: {
            const int64_t *dataArray = fieldEntry->getArrayLong(columnCount);
            publisher.postArrayData(dataArray, 1, columnCount);
        }
        break;
        /** Float array. C++: float[] */
        case cmw::data::DT_FLOAT_ARRAY: {
            const float *dataArray = fieldEntry->getArrayFloat(columnCount);
            publisher.postArrayData(dataArray, 1, columnCount);
        }
        break;
        /** Double array. C++: double[] */
        case cmw::data::DT_DOUBLE_ARRAY: {
            const double *dataArray = fieldEntry->getArrayDouble(columnCount);
            publisher.postArrayData(dataArray, 1, columnCount);
        }
        break;
        /** String array. C++: const char* [] */
        case cmw::data::DT_STRING_ARRAY: {
            const char **dataArray = fieldEntry->getArrayString(columnCount);
            publisher.postArrayData(dataArray, 1, columnCount);
        }
        break;

        /*********************************
         *  ARRAY 2D
         *********************************/
        /** Boolean array 2D. C++: bool[][] */
        case cmw::data::DT_BOOL_ARRAY_2D: {
            const bool *dataArray = fieldEntry->getArrayBool2D(rowCount,
                                                               columnCount);
            publisher.postArrayData(reinterpret_cast<const int8_t *>(dataArray),
                                    rowCount, columnCount);
        }
        break;
        /** Byte array 2D. C++: int8_t[][] */
        case cmw::data::DT_BYTE_ARRAY_2D: {
            const int8_t *dataArray = fieldEntry->getArrayByte2D(rowCount,
                                                                 columnCount);
            publisher.postArrayData(reinterpret_cast<const char *>(dataArray),
                                    rowCount, columnCount);
        }
        break;
        /** Short array 2D. C++: int16_t[][] */
        case cmw::data::DT_SHORT_ARRAY_2D: {
            const int16_t *dataArray = fieldEntry->getArrayShort2D(rowCount,
                                                                   columnCount);
            publisher.postArrayData(dataArray, rowCount, columnCount);
        }
        break;
        /** Int array 2D. C++: int32_t[][] */
        case cmw::data::DT_INT_ARRAY_2D: {
            const int32_t *dataArray = fieldEntry->getArrayInt2D(rowCount,
                                                                 columnCount);
            publisher.postArrayData(dataArray, rowCount, columnCount);
        }
        break;
        /** Long array 2D. C++: int64_t[][] */
        case cmw::data::DT_LONG_ARRAY_2D: {
            const int64_t *dataArray = fieldEntry->getArrayLong2D(rowCount,
                                                                  columnCount);
            publisher.postArrayData(dataArray, rowCount, columnCount);
        }
        break;
        /** Float array 2D. C++: float[][] */
        case cmw::data::DT_FLOAT_ARRAY_2D: {
            const float *dataArray = fieldEntry->getArrayFloat2D(rowCount,
                                                                 columnCount);
            publisher.postArrayData(dataArray, rowCount, columnCount);
        }
        break;
        /* Double array 2D. C++: double[][] */
        case cmw::data::DT_DOUBLE_ARRAY_2D: {
            const double *dataArray = fieldEntry->getArrayDouble2D(rowCount,
                                                                   columnCount);
            publisher.postArrayData(dataArray, rowCount, columnCount);
        }
        break;
        /** String array 2D. C++: const char* [][] */
        case cmw::data::DT_STRING_ARRAY_2D: {
            const char **dataArray = fieldEntry->getArrayString2D(rowCount,
                                                                  columnCount);
            publisher.postArrayData(dataArray, rowCount, columnCount);
        }
        break;
        default:
            LOG(ERROR) << "Type not supported for field " << field.name
                       << " on " << m_config.device << "/" << m_config.property;
            // Clean the service map
            m_services.erase(field.name);
        }
    }
}

void Source::errorReceived(
    const Subscription & /*subscription*/,
    std::unique_ptr<cmw::rda3::common::RdaException> exception,
    cmw::rda3::common::UpdateType /*updateType*/)
{
    LOG(ERROR) << "Error received from " << m_config.device << " : "
               << exception->what();
    // TODO Should we update the DIM Service with something?
}

} // namespace proxy
} // namespace ntof
